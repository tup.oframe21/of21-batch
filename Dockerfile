FROM 220.90.208.154:9125/of21/batch-runtime:21.0.5

#ENV
ARG appName="OF21-Batch"
##########################################################################

USER oflab


WORKDIR /BATCH_HOME

ADD --chown=oflab:oflab target/batch-context.xml batch-context.xml
ADD --chown=oflab:oflab target/run.sh run.sh

COPY --chown=oflab:oflab target/application application
COPY --chown=oflab:oflab target/instream instream
COPY --chown=oflab:oflab target/xml xml
