#!/bin/sh

TARGET_DIR=/home/oflab/tup/sample_repository/migrator_batch

cp -r $TARGET_DIR/jcl/instream target/.
cp -r $TARGET_DIR/jcl/xml target/.
cp -r $TARGET_DIR/cobol/* build-batchApp/src/main/java/.
cp -r $TARGET_DIR/batch-context.xml target/.
