# COBOL template project

COBOL to JAVA 를 통해 생성되는 소스 코드를 해당 프로젝트의 `src/main/java` 하위에 위치시키도록 한다.
COBOL to JAVA 를 수행할 때 지정되는 output path 를 해당 프로젝트의 `src/main/java` 로 지정하면 된다.

## batch

- spring batch 는 `compileOnly` 로 지정되어 있음. PO 환경에 이미 spring batch 가 있다는 가정
- `fatJar` task 를 통해 jar 를 생성할 수 있음
- openframe batch 에서 사용되는 runtime class (DdDO) 를 포함한 jar 를 dependency 에 추가해야함
- `commons-lang`, `big-math` library 는 compile 로 지정되어 있고 jar 를 생성할 때 같이 포함하여 빌드됨.
